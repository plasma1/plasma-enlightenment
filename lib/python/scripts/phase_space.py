import sys, os
sys.path.append(os.path.abspath('../'))

from picongpu.plugins.plot_mpl import PhaseSpaceMPL
import matplotlib.pyplot as plt
from picongpu.plugins.data import PhaseSpaceData
import numpy as np


ps_data = PhaseSpaceData(os.path.abspath('../../../../'))
ps, meta = ps_data.get(species='e', species_filter='all', ps='ypy', iteration=2000)

# create a figure and axes
fig, ax = plt.subplots(1, 1)

# create the visualizer
ps_vis = PhaseSpaceMPL(os.path.abspath('../../../../'), ax)

# plot
ps_vis.visualize(iteration=2000, species='e', ps='ypy')

plt.show()
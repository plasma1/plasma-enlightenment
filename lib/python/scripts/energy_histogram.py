import sys, os
sys.path.append(os.path.abspath('../'))

from picongpu.plugins.data import EnergyHistogramData
from picongpu.plugins.plot_mpl import EnergyHistogramMPL
import matplotlib.pyplot as plt

# load data
eh_data = EnergyHistogramData(os.path.abspath('../../../../'))
counts, bins_keV = eh_data.get('e', species_filter='all', iteration=2000)


# create a figure and axes
fig, ax = plt.subplots(1, 1)

# create the visualizer
eh_vis = EnergyHistogramMPL(os.path.abspath('../../../../'), ax)

eh_vis.visualize(iteration=2000, species='e')

plt.show()
import sys, os
#Change working directory to file directory
os.chdir(os.path.dirname(os.path.abspath(__file__)))
#Add module path
sys.path.append(os.path.abspath('../'))
from picongpu.plugins.data import PNGData
import time
t = time.time()


def get_video_path(dir_path, species, axis):
    SPECIES_LONG_NAMES = {'e': 'Electrons'}
    dir_name = "png" + SPECIES_LONG_NAMES.get(species) + axis.upper()
    output_dir = os.path.join(
            dir_path,
            "simOutput",
            "Videos")
    if not (os.path.exists(output_dir)):
        os.mkdir(output_dir)
    return os.path.join(output_dir,dir_name+'.mp4')

params = {
    "species":"e",
    "axis":"yx"
}
fps = 20


dir_path = os.path.abspath('../../../../')
png_data = PNGData(dir_path)
images_path = png_data.get_data_path(**params)
video_path = get_video_path(dir_path,**params)

create_video = "ffmpeg -y -loglevel quiet -framerate {0} -pattern_type glob -i '{1}/*.png' -pix_fmt yuv420p -c:v h264_nvenc {2}"
create_video = create_video.format(fps,images_path,video_path)
os.system(create_video)
print(time.time()-t)